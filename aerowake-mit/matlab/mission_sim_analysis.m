% Given a bag name and bag sub-path (see scripts/record_sim_mission_bag.sh), provide:
%   - Aerowake commands tracking performance (odometry-based)
%   - ROSflight commands tracking performance (odometry+attitude-based)
%   - Comparison plot:
%      - Air Wrench
%      - Tether Wrench
%      - Fext vs (AW + TW)
%      - odometry vs abs_truth (pose)
%      - odometry vs abs_truth (twist)
%      - rel_odometry vs rel_truth (pose)
%      - rel_odometry vs rel_truth (twist)
%      - sensor health plots
%      - [use/modify existing scripts to assess contributions of individual
%      sensors to estimation schemes, if the estimates seem egregiously off]

% bagname =    'nogpgps';
% bagname =    'novismaybe';
% bagname =    'nozerov';
% bagname =    'nozerov_nobaro';
% bagname    = 'yes_fixedgps';

bagname    = 'full_noisy_t5_zvel';
% bagname    = 'rel_nomag_novel';
bagsubpath = 'sim_missions/spoof';

% bagname =    'head6';
% bagsubpath = 'sim_missions/tracking';

showcontrol = true;
showstates  = true;
showsensors = true;

t0 = 0; % 0;
tf = 100; % inf;

%% FRONTMATTER TODO: VISUALIZE IMU BIASES +++++++++++++++++++++++++++++++++

% close all
addpath(genpath('matlab_utilities/'));
bagfile = strcat('../bags/',bagsubpath,'/',bagname,'.bag');
ymlfile = strcat('../bags/',bagsubpath,'/',bagname,'.yaml');
disp('============================')
bagdata = processAllROSBagTopics(bagfile, false);
ymldata = ReadYaml(ymlfile);
yrfdata = ReadYaml(ymldata.automatic_armer.rosflight_paramfile);
absdata = ReadYaml(ymldata.estimator.param_filename);
reldata = ReadYaml(ymldata.rel_estimator.param_filename);
disp('============================')

%% DATA EXTRACTION

% Wind data
wind_N_mean = ymldata.air_dynamics.wind_x_mean;
wind_E_mean = ymldata.air_dynamics.wind_y_mean;
wind_D_mean = ymldata.air_dynamics.wind_z_mean;

% Estimator usage
abs_use_mocap = absdata.use_mocap; % NO RESIDUALS PROVIDED
abs_use_vision = absdata.use_vision;
abs_use_gnss = absdata.use_gnss;
abs_use_baro = absdata.use_baro;
abs_use_vel = absdata.use_vel;
%
rel_use_mocap = reldata.use_mocap; % NO (VISION) RESIDUALS PROVIDED
rel_use_vision = reldata.use_vision;
rel_use_baro = reldata.use_baro;
rel_use_mag = reldata.use_mag;
rel_use_pos = reldata.use_pos;
rel_use_vel = reldata.use_vel;

% Residuals
if isfield(bagdata, 'att')
    if isfield(bagdata.att, 'abs_res')
        has_abs_att_res = true;
        [idx, abs_att_res_t] = indices_between(bagdata.att.abs_res.throttled.t, t0, tf);
        abs_att_res_phi = bagdata.att.abs_res.throttled.data(1,idx);
        abs_att_res_tht = bagdata.att.abs_res.throttled.data(2,idx);
        abs_att_res_psi = bagdata.att.abs_res.throttled.data(3,idx);
    else
        has_abs_att_res = false;
    end
else
    has_abs_att_res = false;
end
if isfield(bagdata, 'gnss')
    has_abs_gns_res = true;
    [idx, abs_gns_res_t] = indices_between(bagdata.gnss.abs_res.throttled.t, t0, tf);
    abs_gns_res_x = bagdata.gnss.abs_res.throttled.data(1,idx);
    abs_gns_res_y = bagdata.gnss.abs_res.throttled.data(2,idx);
    abs_gns_res_z = bagdata.gnss.abs_res.throttled.data(3,idx);
    abs_gns_res_phi = bagdata.gnss.abs_res.throttled.data(4,idx);
    abs_gns_res_tht = bagdata.gnss.abs_res.throttled.data(5,idx);
    abs_gns_res_psi = bagdata.gnss.abs_res.throttled.data(6,idx);
else
    has_abs_gns_res = false;
end
if isfield(bagdata, 'baro')
    if isfield(bagdata.baro, 'abs_res')
        has_abs_baro_res = true;
        [idx, abs_baro_res_t] = indices_between(bagdata.baro.abs_res.throttled.t, t0, tf);
        abs_baro_res_press = bagdata.baro.abs_res.throttled.data(idx);
    else
        has_abs_baro_res = false;
    end
else
    has_abs_baro_res = false;
end
if isfield(bagdata, 'vel')
    if isfield(bagdata.vel, 'abs_res')
        has_abs_vel_res = true;
        [idx, abs_vel_res_t] = indices_between(bagdata.vel.abs_res.throttled.t, t0, tf);
        abs_vel_res_x = bagdata.vel.abs_res.throttled.data(1,idx);
        abs_vel_res_y = bagdata.vel.abs_res.throttled.data(2,idx);
        abs_vel_res_z = bagdata.vel.abs_res.throttled.data(3,idx);
    else
        has_abs_vel_res = false;
    end
else
    has_abs_vel_res = false;
end
if isfield(bagdata, 'att')
    if isfield(bagdata.att, 'rel_res')
        has_rel_att_res = true;
        [idx, rel_att_res_t] = indices_between(bagdata.att.rel_res.throttled.t, t0, tf);
        rel_att_res_phi = bagdata.att.rel_res.throttled.data(1,idx);
        rel_att_res_tht = bagdata.att.rel_res.throttled.data(2,idx);
        rel_att_res_psi = bagdata.att.rel_res.throttled.data(3,idx);
    else
        has_rel_att_res = false;
    end
else
    has_rel_att_res = false;
end
if isfield(bagdata, 'baro')
    if isfield(bagdata.baro, 'rel_res')
        has_rel_baro_res = true;
        [idx, rel_baro_res_t] = indices_between(bagdata.baro.rel_res.throttled.t, t0, tf);
        rel_baro_res_press = bagdata.baro.rel_res.throttled.data(idx);
    else
        has_rel_baro_res = false;
    end
else
    has_rel_baro_res = false;
end
if isfield(bagdata, 'relative_heading')
    if isfield(bagdata.relative_heading, 'rel_res')
        has_rel_rhd_res = true;
        [idx, rel_rhd_res_t] = indices_between(bagdata.relative_heading.rel_res.throttled.t, t0, tf);
        rel_rhd_res_x = bagdata.relative_heading.rel_res.throttled.data(1,idx);
        rel_rhd_res_y = bagdata.relative_heading.rel_res.throttled.data(2,idx);
        rel_rhd_res_z = bagdata.relative_heading.rel_res.throttled.data(3,idx);
    else
        has_rel_rhd_res = false;
    end
else
    has_rel_rhd_res = false;
end
if isfield(bagdata, 'pos')
    if isfield(bagdata.pos, 'rel_res')
        has_rel_pos_res = true;
        [idx, rel_pos_res_t] = indices_between(bagdata.pos.rel_res.throttled.t, t0, tf);
        rel_pos_res_x = bagdata.pos.rel_res.throttled.data(1,idx);
        rel_pos_res_y = bagdata.pos.rel_res.throttled.data(2,idx);
        rel_pos_res_z = bagdata.pos.rel_res.throttled.data(3,idx);
    else
        has_rel_pos_res = false;
    end
else
    has_rel_pos_res = false;
end
if isfield(bagdata, 'vel')
    if isfield(bagdata.vel, 'rel_res')
        has_rel_vel_res = true;
        [idx, rel_vel_res_t] = indices_between(bagdata.vel.rel_res.throttled.t, t0, tf);
        rel_vel_res_x = bagdata.vel.rel_res.throttled.data(1,idx);
        rel_vel_res_y = bagdata.vel.rel_res.throttled.data(2,idx);
        rel_vel_res_z = bagdata.vel.rel_res.throttled.data(3,idx);
    else
        has_rel_vel_res = false;
    end
else
    has_rel_vel_res = false;
end

% OL controller gains
north_P = ymldata.controller.north_P;
north_I = ymldata.controller.north_I;
north_D = ymldata.controller.north_D;
xdot_P  = ymldata.controller.x_dot_P;
xdot_I  = ymldata.controller.x_dot_I;
xdot_D  = ymldata.controller.x_dot_D;
east_P  = ymldata.controller.east_P;
east_I  = ymldata.controller.east_I;
east_D  = ymldata.controller.east_D;
ydot_P  = ymldata.controller.y_dot_P;
ydot_I  = ymldata.controller.y_dot_I;
ydot_D  = ymldata.controller.y_dot_D;
down_P  = ymldata.controller.down_P;
down_I  = ymldata.controller.down_I;
down_D  = ymldata.controller.down_D;
zdot_P  = ymldata.controller.z_dot_P;
zdot_I  = ymldata.controller.z_dot_I;
zdot_D  = ymldata.controller.z_dot_D;
olpsi_P = ymldata.controller.psi_P;
olpsi_I = ymldata.controller.psi_I;
olpsi_D = ymldata.controller.psi_D;

% OL controller limits
max_xdot = ymldata.controller.max_n_dot;
max_ydot = ymldata.controller.max_e_dot;
max_zdot = ymldata.controller.max_d_dot;
max_phi  = rad_to_deg(ymldata.controller.max_roll);
max_tht  = rad_to_deg(ymldata.controller.max_pitch);
max_r    = rad_to_deg(ymldata.controller.max_yaw_rate);

% Tension threshold
tens_thresh = ymldata.trajectory_planner.tension_threshold;

% ROSflight parameters
for i = 1:1:length(yrfdata)
    RFdatastruct = yrfdata{i};
    if strcmp(RFdatastruct.name, 'PID_PITCH_ANG_P')
        RF_tht_P = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_PITCH_ANG_I')
        RF_tht_I = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_PITCH_ANG_D')
        RF_tht_D = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_ROLL_ANG_P')
        RF_phi_P = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_ROLL_ANG_I')
        RF_phi_I = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_ROLL_ANG_D')
        RF_phi_D = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_YAW_RATE_P')
        RF_r_P = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_YAW_RATE_I')
        RF_r_I = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'PID_YAW_RATE_D')
        RF_r_D = RFdatastruct.value;
    elseif strcmp(RFdatastruct.name, 'RC_MAX_PITCH')
        RF_tht_max = rad_to_deg(RFdatastruct.value);
    elseif strcmp(RFdatastruct.name, 'RC_MAX_ROLL')
        RF_phi_max = rad_to_deg(RFdatastruct.value);
    elseif strcmp(RFdatastruct.name, 'RC_MAX_YAWRATE')
        RF_r_max = rad_to_deg(RFdatastruct.value);
    end
end

% Odometry data
if isfield(bagdata, 'odometry')
    has_odom = true;
    [idx, odom_t] = indices_between(bagdata.odometry.throttled.t,t0,tf);
    odom_N = bagdata.odometry.throttled.pose.position(1,idx);
    odom_N_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(1,idx));
    odom_E = bagdata.odometry.throttled.pose.position(2,idx);
    odom_E_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(8,idx));
    odom_D = bagdata.odometry.throttled.pose.position(3,idx);
    odom_D_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(15,idx));
    [phi, tht, psi] = quat_to_euler(bagdata.odometry.throttled.pose.orientation(4,idx),...
                                    bagdata.odometry.throttled.pose.orientation(1,idx),...
                                    bagdata.odometry.throttled.pose.orientation(2,idx),...
                                    bagdata.odometry.throttled.pose.orientation(3,idx));
    odom_phi = rad_to_deg(phi);
    odom_phi_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(22,idx));
    odom_tht = rad_to_deg(tht);
    odom_tht_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(29,idx));
    odom_psi = rad_to_deg(psi);
    odom_psi_stdev = sqrt(bagdata.odometry.throttled.pose.covariance(36,idx));
    odom_u = bagdata.odometry.throttled.twist.linear(1,idx);
    odom_u_stdev = sqrt(bagdata.odometry.throttled.twist.covariance(1,idx));
    odom_v = bagdata.odometry.throttled.twist.linear(2,idx);
    odom_v_stdev = sqrt(bagdata.odometry.throttled.twist.covariance(8,idx));
    odom_w = bagdata.odometry.throttled.twist.linear(3,idx);
    odom_w_stdev = sqrt(bagdata.odometry.throttled.twist.covariance(15,idx));
    odom_p = rad_to_deg(bagdata.odometry.throttled.twist.angular(1,idx));
    odom_q = rad_to_deg(bagdata.odometry.throttled.twist.angular(2,idx));
    odom_r = rad_to_deg(bagdata.odometry.throttled.twist.angular(3,idx));
else
    has_odom = false;
end

if isfield(bagdata, 'abs')
    if isfield(bagdata.abs, 'imu_bias')
        has_abs_imu_bias = true;
        [idx, abs_imu_bias_t] = indices_between(bagdata.abs.imu_bias.throttled.t,t0,tf);
        abs_imu_bias_accel_x = bagdata.abs.imu_bias.throttled.acc(1,idx);
        abs_imu_bias_accel_y = bagdata.abs.imu_bias.throttled.acc(2,idx);
        abs_imu_bias_accel_z = bagdata.abs.imu_bias.throttled.acc(3,idx);
        abs_imu_bias_gyro_x = bagdata.abs.imu_bias.throttled.gyro(1,idx);
        abs_imu_bias_gyro_y = bagdata.abs.imu_bias.throttled.gyro(2,idx);
        abs_imu_bias_gyro_z = bagdata.abs.imu_bias.throttled.gyro(3,idx);
    else
        has_abs_imu_bias = false;
    end
else
    has_abs_imu_bias = false;
end

if isfield(bagdata, 'rel')
    if isfield(bagdata.rel, 'imu_bias')
        has_rel_imu_bias = true;
        [idx, rel_imu_bias_t] = indices_between(bagdata.rel.imu_bias.throttled.t,t0,tf);
        rel_imu_bias_accel_x = bagdata.rel.imu_bias.throttled.acc(1,idx);
        rel_imu_bias_accel_y = bagdata.rel.imu_bias.throttled.acc(2,idx);
        rel_imu_bias_accel_z = bagdata.rel.imu_bias.throttled.acc(3,idx);
        rel_imu_bias_gyro_x = bagdata.rel.imu_bias.throttled.gyro(1,idx);
        rel_imu_bias_gyro_y = bagdata.rel.imu_bias.throttled.gyro(2,idx);
        rel_imu_bias_gyro_z = bagdata.rel.imu_bias.throttled.gyro(3,idx);
    else
        has_rel_imu_bias = false;
    end
else
    has_rel_imu_bias = false;
end

% Relative odometry data
if isfield(bagdata, 'rel_odometry')
    has_rel_odom = true;
    [idx, rel_odom_t] = indices_between(bagdata.rel_odometry.throttled.t,t0,tf);
    rel_odom_N = bagdata.rel_odometry.throttled.pose.position(1,idx);
    rel_odom_E = bagdata.rel_odometry.throttled.pose.position(2,idx);
    rel_odom_D = bagdata.rel_odometry.throttled.pose.position(3,idx);
    [phi, tht, psi] = quat_to_euler(bagdata.rel_odometry.throttled.pose.orientation(4,idx),...
                                    bagdata.rel_odometry.throttled.pose.orientation(1,idx),...
                                    bagdata.rel_odometry.throttled.pose.orientation(2,idx),...
                                    bagdata.rel_odometry.throttled.pose.orientation(3,idx));
    rel_odom_phi = rad_to_deg(phi);
    rel_odom_tht = rad_to_deg(tht);
    rel_odom_psi = rad_to_deg(psi);
    rel_odom_u = bagdata.rel_odometry.throttled.twist.linear(1,idx);
    rel_odom_v = bagdata.rel_odometry.throttled.twist.linear(2,idx);
    rel_odom_w = bagdata.rel_odometry.throttled.twist.linear(3,idx);
    rel_odom_p = rad_to_deg(bagdata.rel_odometry.throttled.twist.angular(1,idx));
    rel_odom_q = rad_to_deg(bagdata.rel_odometry.throttled.twist.angular(2,idx));
    rel_odom_r = rad_to_deg(bagdata.rel_odometry.throttled.twist.angular(3,idx));
else
    has_rel_odom = false;
end

% Absolute truth data
if isfield(bagdata, 'aerowake_uav')
    if isfield(bagdata.aerowake_uav, 'truth')
        has_abs_truth = true;
        [idx, abs_truth_t] = indices_between(bagdata.aerowake_uav.truth.NED.throttled.t,t0,tf);
        abs_truth_N = bagdata.aerowake_uav.truth.NED.throttled.pose.position(1,idx);
        abs_truth_E = bagdata.aerowake_uav.truth.NED.throttled.pose.position(2,idx);
        abs_truth_D = bagdata.aerowake_uav.truth.NED.throttled.pose.position(3,idx);
        [phi, tht, psi] = quat_to_euler(bagdata.aerowake_uav.truth.NED.throttled.pose.orientation(4,idx),...
                                        bagdata.aerowake_uav.truth.NED.throttled.pose.orientation(1,idx),...
                                        bagdata.aerowake_uav.truth.NED.throttled.pose.orientation(2,idx),...
                                        bagdata.aerowake_uav.truth.NED.throttled.pose.orientation(3,idx));
        abs_truth_phi = rad_to_deg(phi);
        abs_truth_tht = rad_to_deg(tht);
        abs_truth_psi = rad_to_deg(psi);
        abs_truth_u = bagdata.aerowake_uav.truth.NED.throttled.twist.linear(1,idx);
        abs_truth_v = bagdata.aerowake_uav.truth.NED.throttled.twist.linear(2,idx);
        abs_truth_w = bagdata.aerowake_uav.truth.NED.throttled.twist.linear(3,idx);
        abs_truth_p = rad_to_deg(bagdata.aerowake_uav.truth.NED.throttled.twist.angular(1,idx));
        abs_truth_q = rad_to_deg(bagdata.aerowake_uav.truth.NED.throttled.twist.angular(2,idx));
        abs_truth_r = rad_to_deg(bagdata.aerowake_uav.truth.NED.throttled.twist.angular(3,idx));
    else
        has_abs_truth = false;
    end
else
    has_abs_truth = false;
end

% Relative truth data
if isfield(bagdata, 'aerowake_uav')
    if isfield(bagdata.aerowake_uav, 'rel_truth')
        has_rel_truth = true;
        [idx, rel_truth_t] = indices_between(bagdata.aerowake_uav.rel_truth.NED.throttled.t,t0,tf);
        rel_truth_N = bagdata.aerowake_uav.rel_truth.NED.throttled.pose.position(1,idx);
        rel_truth_E = bagdata.aerowake_uav.rel_truth.NED.throttled.pose.position(2,idx);
        rel_truth_D = bagdata.aerowake_uav.rel_truth.NED.throttled.pose.position(3,idx);
        [phi, tht, psi] = quat_to_euler(bagdata.aerowake_uav.rel_truth.NED.throttled.pose.orientation(4,idx),...
                                        bagdata.aerowake_uav.rel_truth.NED.throttled.pose.orientation(1,idx),...
                                        bagdata.aerowake_uav.rel_truth.NED.throttled.pose.orientation(2,idx),...
                                        bagdata.aerowake_uav.rel_truth.NED.throttled.pose.orientation(3,idx));
        rel_truth_phi = rad_to_deg(phi);
        rel_truth_tht = rad_to_deg(tht);
        rel_truth_psi = rad_to_deg(psi);
        rel_truth_u = bagdata.aerowake_uav.rel_truth.NED.throttled.twist.linear(1,idx);
        rel_truth_v = bagdata.aerowake_uav.rel_truth.NED.throttled.twist.linear(2,idx);
        rel_truth_w = bagdata.aerowake_uav.rel_truth.NED.throttled.twist.linear(3,idx);
        rel_truth_p = rad_to_deg(bagdata.aerowake_uav.rel_truth.NED.throttled.twist.angular(1,idx));
        rel_truth_q = rad_to_deg(bagdata.aerowake_uav.rel_truth.NED.throttled.twist.angular(2,idx));
        rel_truth_r = rad_to_deg(bagdata.aerowake_uav.rel_truth.NED.throttled.twist.angular(3,idx));
    else
        has_rel_truth = false;
    end
else
    has_rel_truth = false;
end

% ROSflight attitude
if isfield(bagdata, 'attitude')
    has_rflt = true;
    [idx, rflt_t] = indices_between(bagdata.attitude.throttled.t,t0,tf);
    rflt_phi = rad_to_deg(bagdata.attitude.throttled.q_euler(1,idx));
    rflt_tht = rad_to_deg(bagdata.attitude.throttled.q_euler(2,idx));
    rflt_psi = rad_to_deg(bagdata.attitude.throttled.q_euler(3,idx));
    rflt_p = rad_to_deg(bagdata.attitude.throttled.omega(1,idx));
    rflt_q = rad_to_deg(bagdata.attitude.throttled.omega(2,idx));
    rflt_r = rad_to_deg(bagdata.attitude.throttled.omega(3,idx));
else
    has_rflt = false;
end

% Sensor data
if isfield(bagdata, 'uav_truth_NED')
    has_imu_truth = true;
    [idx, imu_truth_t] = indices_between(bagdata.uav_truth_NED.throttled.t,t0,tf);
    imu_truth_acc_x = bagdata.uav_truth_NED.throttled.imu.accel(1,idx);
    imu_truth_acc_y = bagdata.uav_truth_NED.throttled.imu.accel(2,idx);
    imu_truth_acc_z = bagdata.uav_truth_NED.throttled.imu.accel(3,idx);
    imu_truth_gyr_x = bagdata.uav_truth_NED.throttled.imu.gyro(1,idx);
    imu_truth_gyr_y = bagdata.uav_truth_NED.throttled.imu.gyro(2,idx);
    imu_truth_gyr_z = bagdata.uav_truth_NED.throttled.imu.gyro(3,idx);
else
    has_imu_truth = false;
end
if isfield(bagdata, 'imu')
    has_imu = true;
    [idx, imu_t] = indices_between(bagdata.imu.data.throttled.t,t0,tf);
    imu_acc_x = bagdata.imu.data.throttled.acc(1,idx);
    imu_acc_y = bagdata.imu.data.throttled.acc(2,idx);
    imu_acc_z = bagdata.imu.data.throttled.acc(3,idx);
    imu_gyr_x = bagdata.imu.data.throttled.gyro(1,idx);
    imu_gyr_y = bagdata.imu.data.throttled.gyro(2,idx);
    imu_gyr_z = bagdata.imu.data.throttled.gyro(3,idx);
else
    has_imu = false;
end
if isfield(bagdata, 'baro')
    has_baro = true;
    [idx, baro_t] = indices_between(bagdata.baro.throttled.t,t0,tf);
    baro_press = bagdata.baro.throttled.pressure(idx);
    baro_temp = bagdata.baro.throttled.temperature(idx);
else
    has_baro = false;
end
if isfield(bagdata, 'vision_pose')
    has_vis_pose = true;
    vs_sol = bagdata.vision_pose.throttled.sol_status;
    vs_val = bagdata.vision_pose.throttled.dynamically_valid;
    vs_out = bagdata.vision_pose.throttled.outlier;
    vis_idx = logical((vs_sol > 0) .* (vs_val > 0) .* (vs_out == 0));
    [idx_t, ~] = indices_between(bagdata.vision_pose.throttled.t,t0,tf);
    idx = logical(vis_idx .* idx_t);
    vis_pose_t = bagdata.vision_pose.throttled.t(idx);
    vis_pose_x = bagdata.vision_pose.throttled.transform.translation(1,idx);
    vis_pose_y = bagdata.vision_pose.throttled.transform.translation(2,idx);
    vis_pose_z = bagdata.vision_pose.throttled.transform.translation(3,idx);
    [phi, tht, psi] = quat_to_euler(bagdata.vision_pose.throttled.transform.rotation(4,idx),...
                                    bagdata.vision_pose.throttled.transform.rotation(1,idx),...
                                    bagdata.vision_pose.throttled.transform.rotation(2,idx),...
                                    bagdata.vision_pose.throttled.transform.rotation(3,idx));
    vis_pose_phi = rad_to_deg(phi);
    vis_pose_tht = rad_to_deg(tht);
    vis_pose_psi = rad_to_deg(psi);
else
    has_vis_pose = false;
end
if isfield(bagdata, 'rover')
    if isfield(bagdata.rover, 'navpvt')
        has_gps = true;
        [idx, gps_t] = indices_between(bagdata.rover.navpvt.throttled.t,t0,tf);
        gps_lat = deg_to_rad(bagdata.rover.navpvt.throttled.lat(idx));
        gps_lon = deg_to_rad(bagdata.rover.navpvt.throttled.lon(idx));
        gps_alt = bagdata.rover.navpvt.throttled.alt(idx);
        [gps_N, gps_E, gps_D] = lla2ned(gps_lat(1), gps_lon(1), gps_alt(1), gps_lat(idx), gps_lon(idx), gps_alt(idx));
        gps_velN = bagdata.rover.navpvt.throttled.velN(idx);
        gps_velE = bagdata.rover.navpvt.throttled.velE(idx);
        gps_velD = bagdata.rover.navpvt.throttled.velD(idx);
        gps_hstdev = bagdata.rover.navpvt.throttled.horstdev(idx);
        gps_vstdev = bagdata.rover.navpvt.throttled.verstdev(idx);
        gps_sstdev = bagdata.rover.navpvt.throttled.spdstdev(idx);
    else
        has_gps = false;
    end
    if isfield(bagdata.rover, 'navvelned')
        has_gpsvel = true;
        [idx, gpsvel_t] = indices_between(bagdata.rover.navvelned.throttled.t,t0,tf);
        gpsvel_N = bagdata.rover.navvelned.throttled.velN(idx);
        gpsvel_E = bagdata.rover.navvelned.throttled.velE(idx);
        gpsvel_D = bagdata.rover.navvelned.throttled.velD(idx);
        gpsvel_stdev = bagdata.rover.navvelned.throttled.velstdev(idx);
    else
        has_gpsvel = false;
    end
    if isfield(bagdata.rover, 'navrelposned')
        has_dgps = true;
        [idx, dgps_t] = indices_between(bagdata.rover.navrelposned.throttled.t,t0,tf);
        dgps_N = bagdata.rover.navrelposned.throttled.N(idx);
        dgps_E = bagdata.rover.navrelposned.throttled.E(idx);
        dgps_D = bagdata.rover.navrelposned.throttled.D(idx);
        dgps_Nstdev = bagdata.rover.navrelposned.throttled.Nstdev(idx);
        dgps_Estdev = bagdata.rover.navrelposned.throttled.Estdev(idx);
        dgps_Dstdev = bagdata.rover.navrelposned.throttled.Dstdev(idx);
    else
        has_dgps = false;
    end
else
    has_gps = false;
    has_gpsvel = false;
    has_dgps = false;
end
if isfield(bagdata, 'ship_mag')
    has_smag = true;
    [idx, smag_t] = indices_between(bagdata.ship_mag.throttled.t,t0,tf);
    smag_x = bagdata.ship_mag.throttled.mag(1,idx);
    smag_y = bagdata.ship_mag.throttled.mag(2,idx);
    smag_z = bagdata.ship_mag.throttled.mag(3,idx);
    smag_xstdev = sqrt(bagdata.ship_mag.throttled.cov(1,idx));
    smag_ystdev = sqrt(bagdata.ship_mag.throttled.cov(5,idx));
    smag_zstdev = sqrt(bagdata.ship_mag.throttled.cov(9,idx));
else
    has_smag = false;
end

% Sensor parameters
acc_stdev     = ymldata.rosflight_sil.acc_stdev;
acc_b_rng     = ymldata.rosflight_sil.acc_bias_range;
acc_b_stdev   = ymldata.rosflight_sil.acc_bias_walk_stdev;
gyro_stdev    = ymldata.rosflight_sil.gyro_stdev;
gyro_b_rng    = ymldata.rosflight_sil.gyro_bias_range;
gyro_b_stdev  = ymldata.rosflight_sil.gyro_bias_walk_stdev;
baro_stdev    = ymldata.rosflight_sil.baro_stdev;
baro_b_rng    = ymldata.rosflight_sil.baro_bias_range;
baro_b_stdev  = ymldata.rosflight_sil.baro_bias_walk_stdev;
if has_vis_pose
    pixel_stdev   = ymldata.sim_cam.pixel_stdev;
end
if has_gps
    agps_N_stdev  = ymldata.rover.abs_north_stdev;
    agps_E_stdev  = ymldata.rover.abs_east_stdev;
    agps_A_stdev  = ymldata.rover.abs_alt_stdev;
    agps_N_k      = ymldata.rover.abs_north_k_GPS;
    agps_E_k      = ymldata.rover.abs_east_k_GPS;
    agps_A_k      = ymldata.rover.abs_alt_k_GPS;
    agps_Nv_stdev = ymldata.rover.abs_north_vel_stdev;
    agps_Ev_stdev = ymldata.rover.abs_east_vel_stdev;
    agps_Dv_stdev = ymldata.rover.abs_down_vel_stdev;
end
if has_dgps
    dgps_N_stdev  = ymldata.rover.rel_north_stdev;
    dgps_E_stdev  = ymldata.rover.rel_east_stdev;
    dgps_D_stdev  = ymldata.rover.rel_down_stdev;
end
if has_smag
    mag_stdev     = ymldata.base_compass.mag_stdev;
    mag_b_rng     = ymldata.base_compass.mag_bias_range;
    mag_b_stdev   = ymldata.base_compass.mag_bias_walk_stdev;
end

% Aerowake controller commands TODO: see which fields are interesting
if isfield(bagdata, 'full_hl_command')
    has_olcm = true;
    [idx, olcm_t] = indices_between(bagdata.full_hl_command.throttled.t,t0,tf);
    olcm_N = bagdata.full_hl_command.throttled.pose.position(1,idx);
    olcm_E = bagdata.full_hl_command.throttled.pose.position(2,idx);
    olcm_D = bagdata.full_hl_command.throttled.pose.position(3,idx);
    olcm_phi = rad_to_deg(bagdata.full_hl_command.throttled.pose.orientation(1,idx));
    olcm_tht = rad_to_deg(bagdata.full_hl_command.throttled.pose.orientation(2,idx));
    olcm_psi = rad_to_deg(bagdata.full_hl_command.throttled.pose.orientation(3,idx));
    olcm_u = bagdata.full_hl_command.throttled.twist.linear(1,idx);
    olcm_v = bagdata.full_hl_command.throttled.twist.linear(2,idx);
    olcm_w = bagdata.full_hl_command.throttled.twist.linear(3,idx);
    olcm_p = rad_to_deg(bagdata.full_hl_command.throttled.twist.angular(1,idx));
    olcm_q = rad_to_deg(bagdata.full_hl_command.throttled.twist.angular(2,idx));
    olcm_r = rad_to_deg(bagdata.full_hl_command.throttled.twist.angular(3,idx));
else
    has_olcm = false;
end

% ROSflight inner-loop commands
if isfield(bagdata, 'command')
    has_ilcm = true;
    [idx, ilcm_t] = indices_between(bagdata.command.throttled.t,t0,tf);
    ilcm_phi = rad_to_deg(bagdata.command.throttled.x(idx));
    ilcm_tht = rad_to_deg(bagdata.command.throttled.y(idx));
    ilcm_r = rad_to_deg(bagdata.command.throttled.z(idx));
    ilcm_F = bagdata.command.throttled.F(idx);
else
    has_ilcm = false;
end

% Air wrench (body-frame)
if isfield(bagdata, 'air_wrench')
    has_airw = true;
    [idx, airw_t] = indices_between(bagdata.air_wrench.throttled.t,t0,tf);
    airw_Fx = bagdata.air_wrench.throttled.force(1,idx);
    airw_Fy = bagdata.air_wrench.throttled.force(2,idx);
    airw_Fz = bagdata.air_wrench.throttled.force(3,idx);
    airw_Tx = bagdata.air_wrench.throttled.torque(1,idx);
    airw_Ty = bagdata.air_wrench.throttled.torque(2,idx);
    airw_Tz = bagdata.air_wrench.throttled.torque(3,idx);
else
    has_airw = false;
end

% Tether wrench (body-frame)
if isfield(bagdata, 'tether_wrench')
    has_tetw = true;
    [idx, tetw_t] = indices_between(bagdata.tether_wrench.throttled.t,t0,tf);
    tetw_Fx = bagdata.tether_wrench.throttled.force(1,idx);
    tetw_Fy = bagdata.tether_wrench.throttled.force(2,idx);
    tetw_Fz = bagdata.tether_wrench.throttled.force(3,idx);
    tetw_Tx = bagdata.tether_wrench.throttled.torque(1,idx);
    tetw_Ty = bagdata.tether_wrench.throttled.torque(2,idx);
    tetw_Tz = bagdata.tether_wrench.throttled.torque(3,idx);
else
    has_tetw = false;
end

% External wrench (air + tether) (body-frame)
if isfield(bagdata, 'uav_ext_wrench')
    has_extw = true;
    [idx, extw_t] = indices_between(bagdata.uav_ext_wrench.throttled.t,t0,tf);
    extw_Fx = bagdata.uav_ext_wrench.throttled.force(1,idx);
    extw_Fy = bagdata.uav_ext_wrench.throttled.force(2,idx);
    extw_Fz = bagdata.uav_ext_wrench.throttled.force(3,idx);
    extw_Tx = bagdata.uav_ext_wrench.throttled.torque(1,idx);
    extw_Ty = bagdata.uav_ext_wrench.throttled.torque(2,idx);
    extw_Tz = bagdata.uav_ext_wrench.throttled.torque(3,idx);
else
    has_extw = false;
end

% UKF estimated external wrench (body-frame)
if isfield(bagdata, 'Fext')
    has_Fext = true;
    [idx, Fext_t] = indices_between(bagdata.Fext.throttled.t,t0,tf);
    Fext_Fx = bagdata.Fext.throttled.force(1,idx);
    Fext_Fy = bagdata.Fext.throttled.force(2,idx);
    Fext_Fz = bagdata.Fext.throttled.force(3,idx);
    Fext_Tx = bagdata.Fext.throttled.torque(1,idx);
    Fext_Ty = bagdata.Fext.throttled.torque(2,idx);
    Fext_Tz = bagdata.Fext.throttled.torque(3,idx);
else
    has_Fext = false;
end

%% PLOT LOGIC

plot11  = has_odom && has_olcm;
plot12  = has_odom && has_olcm;
plot13  = has_odom && has_olcm;
plot14  = has_odom && has_olcm;
plot15  = true;
plot16  = true;
plot17  = has_odom && has_ilcm;
plot18  = has_odom && has_ilcm;
plot19  = has_odom && has_ilcm;
plot110 = true;
plot111 = has_airw;
plot112 = has_tetw;
plot113 = has_extw && has_Fext;
plot114 = has_airw;
plot115 = has_tetw;
plot116 = has_extw && has_Fext;
plot21  = has_abs_truth && has_odom;
plot22  = has_abs_truth && has_odom;
plot23  = has_abs_truth && has_odom;
plot24  = has_abs_truth && has_odom;
plot25  = has_abs_truth && has_odom;
plot26  = has_abs_truth && has_odom;
plot27  = has_abs_truth && has_odom;
plot28  = has_abs_truth && has_odom;
plot29  = has_abs_truth && has_odom;
plot210 = has_abs_truth && has_odom;
plot211 = has_abs_truth && has_odom;
plot212 = has_abs_truth && has_odom;
plot213 = has_rel_truth && has_rel_odom;
plot214 = has_rel_truth && has_rel_odom;
plot215 = has_rel_truth && has_rel_odom;
plot216 = has_rel_truth && has_rel_odom;
plot217 = has_rel_truth && has_rel_odom;
plot218 = has_rel_truth && has_rel_odom;
plot219 = has_rel_truth && has_rel_odom;
plot220 = has_rel_truth && has_rel_odom;
plot221 = has_rel_truth && has_rel_odom;
plot222 = has_rel_truth && has_rel_odom;
plot223 = has_rel_truth && has_rel_odom;
plot224 = has_rel_truth && has_rel_odom;
plot31  = has_imu_truth && has_imu;
plot32  = has_imu_truth && has_imu;
plot33  = has_baro;
plot34  = has_rel_truth && has_vis_pose;
plot35  = has_rel_truth && has_vis_pose;
plot36  = has_abs_truth && has_gps;
plot37  = has_gps;
plot38  = has_rel_truth && has_dgps;
plot39  = has_gpsvel;
plot310 = has_smag;

%% PLOTS

set(0, 'DefaultLineLineWidth', 1.25);
red_sig_color = [255 100 100]./255;
grn_sig_color = [100 255 100]./255;
blu_sig_color = [100 100 255]./255;
blk_sig_color = [  0   0   0]./255;

if showcontrol

    % TAB GROUP: CONTROL + UKF
    f1 = figure('Position',[0 0 2000 1000],'Name',strcat(bagname,': CONTROL + UKF'),'NumberTitle','off');
    tabgp1 = uitabgroup(f1);

    % Aerowake command tracking
    if plot11
        tab11 = uitab(tabgp1,'Title','AW phi Tracking');
        % axes('Parent',tab11);
        % subplot(2,3,1)
        hax11 = axes('Parent',tab11);
        plot(hax11, odom_t, odom_tht, 'k-', olcm_t, olcm_tht, 'r--')
        xlabel('t (s)')
        ylabel('\theta (deg)')
        legend('Actual','Commanded')
        grid on
    end
    % subplot(2,3,2)
    if plot12
        tab12 = uitab(tabgp1,'Title','AW v Tracking');
        hax12 = axes('Parent',tab12);
        plot(odom_t, odom_v, 'k-', olcm_t, olcm_v, 'r--')
        xlabel('t (s)')
        ylabel('v (m/s)')
        grid on
    end
    % subplot(2,3,3)
    if plot13
        tab13 = uitab(tabgp1,'Title','AW D Tracking');
        hax13 = axes('Parent',tab13);
        plot(odom_t, odom_D, 'k-', olcm_t, olcm_D, 'r--')
        xlabel('t (s)')
        ylabel('D (m)')
        grid on
    end
    % subplot(2,3,4)
    if plot14
        tab14 = uitab(tabgp1,'Title','AW psi Tracking');
        hax14 = axes('Parent',tab14);
        plot(odom_t, odom_psi, 'k-', olcm_t, olcm_psi, 'r--')
        xlabel('t (s)')
        ylabel('\psi (deg)')
        grid on
    end
    % hF = subplot(2,3,5);
    if plot15
        tab15 = uitab(tabgp1,'Title','AW Gains');
        hax15 = axes('Parent',tab15);
        set(hax15, 'color', [1 1 1],'visible','off')
        text(0.0, 0.8, ['North PID: [' num2str(north_P) ', ' num2str(north_I) ', ' num2str(north_D) ']'])
        text(0.5, 0.8, ['xdot  PID: [' num2str(xdot_P) ', ' num2str(xdot_I) ', ' num2str(xdot_D) ']'])
        text(0.0, 0.6, ['East  PID: [' num2str(east_P) ', ' num2str(east_I) ', ' num2str(east_D) ']'])
        text(0.5, 0.6, ['ydot  PID: [' num2str(ydot_P) ', ' num2str(ydot_I) ', ' num2str(ydot_D) ']'])
        text(0.0, 0.4, ['Down  PID: [' num2str(down_P) ', ' num2str(down_I) ', ' num2str(down_D) ']'])
        text(0.5, 0.4, ['zdot  PID: [' num2str(zdot_P) ', ' num2str(zdot_I) ', ' num2str(zdot_D) ']'])
        text(0.0, 0.2, ['\psi  PID: [' num2str(olpsi_P) ', ' num2str(olpsi_I) ', ' num2str(olpsi_D) ']'])
    end
    % hF2 = subplot(2,3,6);
    if plot16
        tab16 = uitab(tabgp1,'Title','AW Limits');
        hax16 = axes('Parent',tab16);
        set(hax16, 'color', [1 1 1],'visible','off')
        text(0.0, 0.7, ['Max xdot: ' num2str(max_xdot)])
        text(0.0, 0.5, ['Max ydot: ' num2str(max_ydot)])
        text(0.0, 0.3, ['Max zdot: ' num2str(max_zdot)])
        text(0.5, 0.7, ['Max \phi (deg): ' num2str(max_phi)])  
        text(0.5, 0.5, ['Max \theta (deg): ' num2str(max_tht)])
        text(0.5, 0.3, ['Max r (deg/s): ' num2str(max_r)])
    end

    % % ROSflight command tracking
    if plot17
        tab17 = uitab(tabgp1,'Title','RF phi Tracking');
        hax17 = axes('Parent',tab17);
        plot(rflt_t, rflt_phi, 'k-', ilcm_t, ilcm_phi, 'r--')
        xlabel('t (s)')
        ylabel('\phi (deg)')
        legend('Actual','Commanded')
        grid on
    end
    % subplot(2,2,2)
    if plot18
        tab18 = uitab(tabgp1,'Title','RF theta Tracking');
        hax18 = axes('Parent',tab18);
        plot(rflt_t, rflt_tht, 'k-', ilcm_t, ilcm_tht, 'r--')
        xlabel('t (s)')
        ylabel('\theta (deg)')
        grid on
    end
    % subplot(2,2,3)
    if plot19
        tab19 = uitab(tabgp1,'Title','RF r Tracking');
        hax19 = axes('Parent',tab19);
        plot(rflt_t, rflt_r, 'k-', ilcm_t, ilcm_r, 'r--')
        xlabel('t (s)')
        ylabel('r (deg/s)')
        grid on
    end
    % hF3 = subplot(2,2,4);
    if plot110
        tab110 = uitab(tabgp1,'Title','RF Params');
        hax110 = axes('Parent',tab110);
        set(hax110, 'color', [1 1 1], 'visible', 'off')
        text(0.0, 0.7, ['\phi PID: [' num2str(RF_phi_P) ', ' num2str(RF_phi_I) ', ' num2str(RF_phi_D) ']'])
        text(0.0, 0.5, ['\theta  PID: [' num2str(RF_tht_P) ', ' num2str(RF_tht_I) ', ' num2str(RF_tht_D) ']'])
        text(0.0, 0.3, ['r  PID: [' num2str(RF_r_P) ', ' num2str(RF_r_I) ', ' num2str(RF_r_D) ']'])
        text(0.5, 0.7, ['\phi max (deg): ' num2str(RF_phi_max)])
        text(0.5, 0.5, ['\theta max (deg): ' num2str(RF_tht_max)])
        text(0.5, 0.3, ['r max (deg/s): ' num2str(RF_r_max)])
    end

    % % External force plot
    if plot111
        tab111 = uitab(tabgp1,'Title','Fext Aero');
        hax111 = axes('Parent',tab111);
        plot(airw_t, airw_Fx, 'r-', airw_t, airw_Fy, 'g-', airw_t, airw_Fz, 'b-')
        xlabel('t (s)')
        ylabel('Aerodynamic F (N)')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        legend('F_x','F_y','F_z')
        grid on
    end
    % subplot(2,3,2)
    if plot112
        tab112 = uitab(tabgp1,'Title','Fext Tether');
        hax112 = axes('Parent',tab112);
        plot(tetw_t, tetw_Fx, 'r-', tetw_t, tetw_Fy, 'g-', tetw_t, tetw_Fz, 'b-')
        xlabel('t (s)')
        ylabel('Tether F (N)')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        legend('F_x','F_y','F_z')
        grid on
    end
    % subplot(2,3,3)
    if plot113
        tab113 = uitab(tabgp1,'Title','Fext UKF');
        hax113 = axes('Parent',tab113);
        plot(extw_t, extw_Fx, 'r-', extw_t, extw_Fy, 'g-', extw_t, extw_Fz, 'b-', ...
             Fext_t, Fext_Fx,'r--', Fext_t, Fext_Fy,'g--', Fext_t, Fext_Fz,'b--', ...
             [extw_t(1) extw_t(end)], [tens_thresh tens_thresh], 'k-')
        xlabel('t (s)')
        ylabel('External F (N)')
        legend('Actual F_x','Actual F_y','Actual F_z','Estimated F_x','Estimated F_y','Estimated F_z','Tether Tension Threshold','Location','Southeast')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        grid on
    end
    % subplot(2,3,4)
    if plot114
        tab114 = uitab(tabgp1,'Title','Text Aero');
        hax114 = axes('Parent',tab114);
        plot(airw_t, airw_Tx, 'r-', airw_t, airw_Ty, 'g-', airw_t, airw_Tz, 'b-')
        xlabel('t (s)')
        ylabel('Aerodynamic T (N-m)')
        legend('T_x','T_y','T_z')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        grid on
    end
    % subplot(2,3,5)
    if plot115
        tab115 = uitab(tabgp1,'Title','Text Tether');
        hax115 = axes('Parent',tab115);
        plot(tetw_t, tetw_Tx, 'r-', tetw_t, tetw_Ty, 'g-', tetw_t, tetw_Tz, 'b-')
        xlabel('t (s)')
        ylabel('Tether T (N)')
        legend('T_x','T_y','T_z')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        grid on
    end
    % subplot(2,3,6)
    if plot116
        tab116 = uitab(tabgp1,'Title','Text UKF');
        hax116 = axes('Parent',tab116);
        plot(extw_t, extw_Tx, 'r-', extw_t, extw_Ty, 'g-', extw_t, extw_Tz, 'b-', ...
             Fext_t, Fext_Tx, 'r--', Fext_t, Fext_Ty, 'g--', Fext_t, Fext_Tz, 'b--')
        xlabel('t (s)')
        ylabel('External T (N-m)')
        legend('T_x','T_y','T_z','Est. T_x','Est. T_y','Est. T_z')
        title(strcat('Mean Wind NED: [',num2str(wind_N_mean),', ',num2str(wind_E_mean),', ',num2str(wind_D_mean),']'))
        grid on
    end
end

% TAB GROUP: STATE ESTIMATES
if showstates
    f2 = figure('Position',[0 1000 2000 1000],'Name',strcat(bagname,': STATE ESTIMATES'),'NumberTitle','off');
    tabgp2 = uitabgroup(f2);

    % subplot(2,3,1)
    if plot21
        tab21 = uitab(tabgp2,'Title','Odom N');
        hax21 = axes('Parent',tab21);
        plot(abs_truth_t, abs_truth_N, 'k-'); hold on
        plot_sigma_line(odom_t, odom_N, odom_t, odom_N_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('N (m)')
        grid on
        legend('Truth','Estimated')
    end
    % subplot(2,3,2)
    if plot22
        tab22 = uitab(tabgp2,'Title','Odom E');
        hax22 = axes('Parent',tab22);
        plot(abs_truth_t, abs_truth_E, 'k-'); hold on
        plot_sigma_line(odom_t, odom_E, odom_t, odom_E_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('E (m)')
        grid on
    end
    % subplot(2,3,3)
    if plot23
        tab23 = uitab(tabgp2,'Title','Odom D');
        hax23 = axes('Parent',tab23);
        plot(abs_truth_t, abs_truth_D, 'k-'); hold on
        plot_sigma_line(odom_t, odom_D, odom_t, odom_D_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('D (m)')
        grid on
    end
    % subplot(2,3,4)
    if plot24
        tab24 = uitab(tabgp2,'Title','Odom phi');
        hax24 = axes('Parent',tab24);
        plot(abs_truth_t, abs_truth_phi, 'k-'); hold on
        plot_sigma_line(odom_t, odom_phi, odom_t, odom_phi_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('\phi (deg)')
        grid on
    end
    % subplot(2,3,5)
    if plot25
        tab25 = uitab(tabgp2,'Title','Odom theta');
        hax25 = axes('Parent',tab25);
        plot(abs_truth_t, abs_truth_tht, 'k-'); hold on
        plot_sigma_line(odom_t, odom_tht, odom_t, odom_tht_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('\theta (deg)')
        grid on
    end
    % subplot(2,3,6)
    if plot26
        tab26 = uitab(tabgp2,'Title','Odom psi');
        hax26 = axes('Parent',tab26);
        plot(abs_truth_t, abs_truth_psi, 'k-'); hold on
        plot_sigma_line(odom_t, odom_psi, odom_t, odom_psi_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('\psi (deg)')
        grid on
    end
    % sgtitle('Absolute Estimation (Pose)')
    % 
    % % Absolute estimation plot - twist
    % figure('Position',[0 1000 2000 1000])
    % subplot(2,3,1)
    if plot27
        tab27 = uitab(tabgp2,'Title','Odom u');
        hax27 = axes('Parent',tab27);
        plot(abs_truth_t, abs_truth_u, 'k-'); hold on
        plot_sigma_line(odom_t, odom_u, odom_t, odom_u_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('u (m/s)')
        grid on
        legend('Truth','Estimated')
    end
    % subplot(2,3,2)
    if plot28
        tab28 = uitab(tabgp2,'Title','Odom v');
        hax28 = axes('Parent',tab28);
        plot(abs_truth_t, abs_truth_v, 'k-'); hold on
        plot_sigma_line(odom_t, odom_v, odom_t, odom_v_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('v (m/s)')
        grid on
    end
    % subplot(2,3,3)
    if plot29
        tab29 = uitab(tabgp2,'Title','Odom w');
        hax29 = axes('Parent',tab29);
        plot(abs_truth_t, abs_truth_w, 'k-'); hold on
        plot_sigma_line(odom_t, odom_w, odom_t, odom_w_stdev, false, 'r--', red_sig_color, 1, false)
        xlabel('t(s)')
        ylabel('w (m/s)')
        grid on
    end
    % subplot(2,3,4)
    if plot210
        tab210 = uitab(tabgp2,'Title','Odom p');
        hax210 = axes('Parent',tab210);
        plot(abs_truth_t, abs_truth_p, 'k-', odom_t, odom_p, 'r--')
        xlabel('t(s)')
        ylabel('p (deg/s)')
        grid on
    end
    % subplot(2,3,5)
    if plot211
        tab211 = uitab(tabgp2,'Title','Odom q');
        hax211 = axes('Parent',tab211);
        plot(abs_truth_t, abs_truth_q, 'k-', odom_t, odom_q, 'r--')
        xlabel('t(s)')
        ylabel('q (deg/s)')
        grid on
    end
    % subplot(2,3,6)
    if plot212
        tab212 = uitab(tabgp2,'Title','Odom r');
        hax212 = axes('Parent',tab212);
        plot(abs_truth_t, abs_truth_r, 'k-', odom_t, odom_r, 'r--')
        xlabel('t(s)')
        ylabel('r (deg/s)')
        grid on
    end
    if has_abs_imu_bias
        tab2121 = uitab(tabgp2,'Title','IMU Accel Biases');
        tab2121 = axes('Parent',tab2121);
        plot(abs_imu_bias_t, abs_imu_bias_accel_x, 'r-',...
             abs_imu_bias_t, abs_imu_bias_accel_y, 'g-',...
             abs_imu_bias_t, abs_imu_bias_accel_z, 'b-')
        xlabel('t (s)')
        ylabel('(m/s^2)')
        grid on
        tab2122 = uitab(tabgp2,'Title','IMU Gyro Biases');
        tab2122 = axes('Parent',tab2122);
        plot(abs_imu_bias_t, abs_imu_bias_gyro_x, 'r-',...
             abs_imu_bias_t, abs_imu_bias_gyro_y, 'g-',...
             abs_imu_bias_t, abs_imu_bias_gyro_z, 'b-')
        xlabel('t (s)')
        ylabel('(m/s^2)')
        grid on
    end
    % sgtitle('Absolute Estimation (Twist)')
    % 
    % % Relative estimation plot - pose
    % figure('Position',[0 1000 2000 1000])
    % subplot(2,3,1)
    if plot213
        tab213 = uitab(tabgp2,'Title','Rel Odom N');
        hax213 = axes('Parent',tab213);
        plot(rel_truth_t, rel_truth_N, 'k-', rel_odom_t, rel_odom_N, 'r--')
        xlabel('t(s)')
        ylabel('N (m)')
        grid on
        legend('Truth','Estimated')
    end
    % subplot(2,3,2)
    if plot214
        tab214 = uitab(tabgp2,'Title','Rel Odom E');
        hax214 = axes('Parent',tab214);
        plot(rel_truth_t, rel_truth_E, 'k-', rel_odom_t, rel_odom_E, 'r--')
        xlabel('t(s)')
        ylabel('E (m)')
        grid on
    end
    % subplot(2,3,3)
    if plot215
        tab215 = uitab(tabgp2,'Title','Rel Odom D');
        hax215 = axes('Parent',tab215);
        plot(rel_truth_t, rel_truth_D, 'k-', rel_odom_t, rel_odom_D, 'r--')
        xlabel('t(s)')
        ylabel('D (m)')
        grid on
    end
    % subplot(2,3,4)
    if plot216
        tab216 = uitab(tabgp2,'Title','Rel Odom phi');
        hax216 = axes('Parent',tab216);
        plot(rel_truth_t, rel_truth_phi, 'k-', rel_odom_t, rel_odom_phi, 'r--')
        xlabel('t(s)')
        ylabel('\phi (deg)')
        grid on
    end
    % subplot(2,3,5)
    if plot217
        tab217 = uitab(tabgp2,'Title','Rel Odom theta');
        hax217 = axes('Parent',tab217);
        plot(rel_truth_t, rel_truth_tht, 'k-', rel_odom_t, rel_odom_tht, 'r--')
        xlabel('t(s)')
        ylabel('\theta (deg)')
        grid on
    end
    % subplot(2,3,6)
    if plot218
        tab218 = uitab(tabgp2,'Title','Rel Odom psi');
        hax218 = axes('Parent',tab218);
        plot(rel_truth_t, rel_truth_psi, 'k-', rel_odom_t, rel_odom_psi, 'r--')
        xlabel('t(s)')
        ylabel('\psi (deg)')
        grid on
    end
    % sgtitle('Relative Estimation (Pose)')
    % 
    % % Relative estimation plot - twist
    % figure('Position',[0 1000 2000 1000])
    % subplot(2,3,1)
    if plot219
        tab219 = uitab(tabgp2,'Title','Rel Odom u');
        hax219 = axes('Parent',tab219);
        plot(rel_truth_t, rel_truth_u, 'k-', rel_odom_t, rel_odom_u, 'r--')
        xlabel('t(s)')
        ylabel('u (m/s)')
        grid on
        legend('Truth','Estimated')
    end
    % subplot(2,3,2)
    if plot220
        tab220 = uitab(tabgp2,'Title','Rel Odom v');
        hax220 = axes('Parent',tab220);
        plot(rel_truth_t, rel_truth_v, 'k-', rel_odom_t, rel_odom_v, 'r--')
        xlabel('t(s)')
        ylabel('v (m/s)')
        grid on
    end
    % subplot(2,3,3)
    if plot221
        tab221 = uitab(tabgp2,'Title','Rel Odom w');
        hax221 = axes('Parent',tab221);
        plot(rel_truth_t, rel_truth_w, 'k-', rel_odom_t, rel_odom_w, 'r--')
        xlabel('t(s)')
        ylabel('w (m/s)')
        grid on
    end
    % subplot(2,3,4)
    if plot222
        tab222 = uitab(tabgp2,'Title','Rel Odom p');
        hax222 = axes('Parent',tab222);
        plot(rel_truth_t, rel_truth_p, 'k-', rel_odom_t, rel_odom_p, 'r--')
        xlabel('t(s)')
        ylabel('p (deg/s)')
        grid on
    end
    % subplot(2,3,5)
    if plot223
        tab223 = uitab(tabgp2,'Title','Rel Odom q');
        hax223 = axes('Parent',tab223);
        plot(rel_truth_t, rel_truth_q, 'k-', rel_odom_t, rel_odom_q, 'r--')
        xlabel('t(s)')
        ylabel('q (deg/s)')
        grid on
    end
    % subplot(2,3,6)
    if plot224
        tab224 = uitab(tabgp2,'Title','Rel Odom r');
        hax224 = axes('Parent',tab224);
        plot(rel_truth_t, rel_truth_r, 'k-', rel_odom_t, rel_odom_r, 'r--')
        xlabel('t(s)')
        ylabel('r (deg/s)')
        grid on
    end
    if has_rel_imu_bias
        tab2241 = uitab(tabgp2,'Title','IMU Accel Biases');
        tab2241 = axes('Parent',tab2241);
        plot(abs_imu_bias_t, abs_imu_bias_accel_x, 'r-',...
             abs_imu_bias_t, abs_imu_bias_accel_y, 'g-',...
             abs_imu_bias_t, abs_imu_bias_accel_z, 'b-')
        xlabel('t (s)')
        ylabel('(m/s^2)')
        grid on
        tab2242 = uitab(tabgp2,'Title','IMU Gyro Biases');
        tab2242 = axes('Parent',tab2242);
        plot(abs_imu_bias_t, abs_imu_bias_gyro_x, 'r-',...
             abs_imu_bias_t, abs_imu_bias_gyro_y, 'g-',...
             abs_imu_bias_t, abs_imu_bias_gyro_z, 'b-')
        xlabel('t (s)')
        ylabel('(m/s^2)')
        grid on
    end
end
% sgtitle('Relative Estimation (Twist)')

% TAB GROUP: SENSORS
if showsensors
    f3 = figure('Position',[2000 1000 2000 1000],'Name',strcat(bagname,': SENSORS'),'NumberTitle','off');
    tabgp3 = uitabgroup(f3);
    % 
    if plot31
        tab31 = uitab(tabgp3,'Title','IMU Accels');
        hax31 = axes('Parent',tab31);
        plot(imu_truth_t, imu_truth_acc_x, 'r-', imu_t, imu_acc_x, 'r--',...
             imu_truth_t, imu_truth_acc_y, 'g-', imu_t, imu_acc_y, 'g--',...
             imu_truth_t, imu_truth_acc_z, 'b-', imu_t, imu_acc_z, 'b--')
        xlabel('t (s)')
        ylabel('(m/s^2)')
        grid on
        title(sprintf('STDEV: %0.5f    BIAS RANGE: %0.5f    BIAS WALK STDEV: %0.5f', acc_stdev, acc_b_rng, acc_b_stdev))
    end
    %
    if plot32
        tab32 = uitab(tabgp3,'Title','IMU Gyros');
        hax32 = axes('Parent',tab32);
        plot(imu_truth_t, imu_truth_gyr_x, 'r-', imu_t, imu_gyr_x, 'r--',...
             imu_truth_t, imu_truth_gyr_y, 'g-', imu_t, imu_gyr_y, 'g--',...
             imu_truth_t, imu_truth_gyr_z, 'b-', imu_t, imu_gyr_z, 'b--')
        xlabel('t (s)')
        ylabel('(rad/s)')
        grid on
        title(sprintf('STDEV: %0.5f    BIAS RANGE: %0.5f    BIAS WALK STDEV: %0.5f', gyro_stdev, gyro_b_rng, gyro_b_stdev))
    end
    %
    if plot33
        tab33 = uitab(tabgp3,'Title','Baro');
        hax33 = axes('Parent',tab33);
        plot(baro_t, baro_press, 'k-')
        grid on
        
        if has_abs_baro_res
            hold on
            if abs_use_baro
                plot(abs_baro_res_t, abs_baro_res_press, 'b+')
            else
                plot(abs_baro_res_t, abs_baro_res_press, 'bo')
            end
            hold off
        end
        
        if has_rel_baro_res
            hold on
            if rel_use_baro
                plot(rel_baro_res_t, rel_baro_res_press, 'r+')
            else
                plot(rel_baro_res_t, rel_baro_res_press, 'ro')
            end
            hold off
        end
        
        xlabel('t (s)')
        ylabel('Pressure')
        title(sprintf('STDEV: %0.5f    BIAS RANGE: %0.5f    BIAS WALK STDEV: %0.5f', baro_stdev, baro_b_rng, baro_b_stdev))
    end
    %
    if plot34
        tab34 = uitab(tabgp3,'Title','Vision Pos');
        hax34 = axes('Parent',tab34);
        plot(rel_truth_t, rel_truth_N, 'r-', vis_pose_t, vis_pose_x, 'r*',...
             rel_truth_t, rel_truth_E, 'g-', vis_pose_t, vis_pose_y, 'g*',...
             rel_truth_t, rel_truth_D, 'b-', vis_pose_t, vis_pose_z, 'b*')
        xlabel('t (s)')
        ylabel('(m)')
        grid on
        title(sprintf('PIXEL STDEV: %0.5f', pixel_stdev))
    end
    %
    if plot35
        tab35 = uitab(tabgp3,'Title','Vision Att');
        hax35 = axes('Parent',tab35);
        plot(rel_truth_t, rel_truth_phi, 'r-', vis_pose_t, vis_pose_phi, 'r*',...
             rel_truth_t, rel_truth_tht, 'g-', vis_pose_t, vis_pose_tht, 'g*',...
             rel_truth_t, rel_truth_psi, 'b-', vis_pose_t, vis_pose_psi, 'b*')
        xlabel('t (s)')
        ylabel('(deg)')
        grid on
        
        if has_abs_att_res
            hold on
            if abs_use_vision
                plot(abs_att_res_t, abs_att_res_phi, 'r+',...
                     abs_att_res_t, abs_att_res_tht, 'g+',...
                     abs_att_res_t, abs_att_res_psi, 'b+')
            else
                plot(abs_att_res_t, abs_att_res_phi, 'ro',...
                     abs_att_res_t, abs_att_res_tht, 'go',...
                     abs_att_res_t, abs_att_res_psi, 'bo')
            end
            hold off
        end
        if has_rel_att_res
            hold on
            if rel_use_vision
                plot(rel_att_res_t, rel_att_res_phi, 'rx',...
                     rel_att_res_t, rel_att_res_tht, 'gx',...
                     rel_att_res_t, rel_att_res_psi, 'bx')
            else
                plot(rel_att_res_t, rel_att_res_phi, 'rd',...
                     rel_att_res_t, rel_att_res_tht, 'gd',...
                     rel_att_res_t, rel_att_res_psi, 'bd')
            end
            hold off
        end
    end
    %
    if plot36
        tab36 = uitab(tabgp3,'Title','Abs GPS Pos');
        hax36 = axes('Parent',tab36);
        plot(abs_truth_t, abs_truth_N, 'r-',...
             abs_truth_t, abs_truth_E, 'g-',...
             abs_truth_t, abs_truth_D, 'b-')
        grid on; hold on
        plot_sigma_line(gps_t, gps_N, gps_t, gps_hstdev, false, 'r*', red_sig_color, 1, true)
        plot_sigma_line(gps_t, gps_E, gps_t, gps_hstdev, false, 'g*', grn_sig_color, 1, true)
        plot_sigma_line(gps_t, gps_D, gps_t, gps_vstdev, false, 'b*', blu_sig_color, 1, false)
        
        if has_abs_gns_res
            hold on
            if abs_use_gnss
                plot(abs_gns_res_t, abs_gns_res_x, 'r+',...
                     abs_gns_res_t, abs_gns_res_y, 'g+',...
                     abs_gns_res_t, abs_gns_res_z, 'b+')
            else
                plot(abs_gns_res_t, abs_gns_res_x, 'ro',...
                     abs_gns_res_t, abs_gns_res_y, 'go',...
                     abs_gns_res_t, abs_gns_res_z, 'bo')
            end
            hold off
        end
        
        xlabel('t (s)')
        ylabel('(m)')
        title(sprintf('N STDEV: %0.5f    N k: %0.5f    E STDEV: %0.5f    E k: %0.5f    A STDEV: %0.5f    A k: %0.5f', agps_N_stdev, agps_N_k, agps_E_stdev, agps_E_k, agps_A_stdev, agps_A_k))
    end
    %
    if plot37
        tab37 = uitab(tabgp3,'Title','Abs GPS Vel');
        hax37 = axes('Parent',tab37);
        plot_sigma_line(gps_t, gps_velN, gps_t, gps_sstdev, false, 'r-*', red_sig_color, 1, true)
        plot_sigma_line(gps_t, gps_velE, gps_t, gps_sstdev, false, 'g-*', grn_sig_color, 1, true)
        plot_sigma_line(gps_t, gps_velD, gps_t, gps_sstdev, false, 'b-*', blu_sig_color, 1, false)
        
        if has_abs_vel_res
            hold on
            if abs_use_vel
                plot(abs_vel_res_t, abs_vel_res_x, 'r+',...
                     abs_vel_res_t, abs_vel_res_y, 'g+',...
                     abs_vel_res_t, abs_vel_res_z, 'b+')
            else
                plot(abs_vel_res_t, abs_vel_res_x, 'ro',...
                     abs_vel_res_t, abs_vel_res_y, 'go',...
                     abs_vel_res_t, abs_vel_res_z, 'bo')
            end
            hold off
        end
        
        xlabel('t (s)')
        ylabel('(m/s)')
        grid on
        title(sprintf('N VEL STDEV: %0.5f    E VEL STDEV: %0.5f    D VEL STDEV: %0.5f', agps_Nv_stdev, agps_Ev_stdev, agps_Dv_stdev))
    end
    %
    if plot38
        tab38 = uitab(tabgp3,'Title','Rel GPS Pos');
        hax38 = axes('Parent',tab38);
        plot(rel_truth_t, rel_truth_N, 'r-', rel_truth_t, rel_truth_E, 'g-', rel_truth_t, rel_truth_D, 'b-')
        grid on; hold on
        xlabel('t (s)')
        ylabel('(m)')
        plot_sigma_line(dgps_t, dgps_N, dgps_t, dgps_Nstdev, false, 'r*', red_sig_color, 1, true)
        plot_sigma_line(dgps_t, dgps_E, dgps_t, dgps_Estdev, false, 'g*', grn_sig_color, 1, true)
        plot_sigma_line(dgps_t, dgps_D, dgps_t, dgps_Nstdev, false, 'b*', blu_sig_color, 1, false)
        
        if has_rel_pos_res
            hold on
            if rel_use_pos
                plot(rel_pos_res_t, rel_pos_res_x, 'r+',...
                     rel_pos_res_t, rel_pos_res_y, 'g+',...
                     rel_pos_res_t, rel_pos_res_z, 'b+')
            else
                plot(rel_pos_res_t, rel_pos_res_x, 'ro',...
                     rel_pos_res_t, rel_pos_res_y, 'go',...
                     rel_pos_res_t, rel_pos_res_z, 'bo')
            end
            hold off
        end
        
        title(sprintf('REL N STDEV: %0.5f    REL E STDEV: %0.5f    REL D STDEV: %0.5f', dgps_N_stdev, dgps_E_stdev, dgps_D_stdev))
    end
    %
    if plot39
        tab39 = uitab(tabgp3,'Title','Rel GPS Vel');
        hax39 = axes('Parent',tab39);
        plot_sigma_line(gpsvel_t, gpsvel_N, gpsvel_t, gpsvel_stdev, false, 'r-*', red_sig_color, 1, true)
        plot_sigma_line(gpsvel_t, gpsvel_E, gpsvel_t, gpsvel_stdev, false, 'g-*', grn_sig_color, 1, true)
        plot_sigma_line(gpsvel_t, gpsvel_D, gpsvel_t, gpsvel_stdev, false, 'b-*', blu_sig_color, 1, false)
        
        if has_abs_vel_res
            hold on
            if abs_use_vel
                plot(abs_vel_res_t, abs_vel_res_x, 'r+',...
                     abs_vel_res_t, abs_vel_res_y, 'g+',...
                     abs_vel_res_t, abs_vel_res_z, 'b+')
            else
                plot(abs_vel_res_t, abs_vel_res_x, 'ro',...
                     abs_vel_res_t, abs_vel_res_y, 'go',...
                     abs_vel_res_t, abs_vel_res_z, 'bo')
            end
            hold off
        end
        if has_rel_vel_res
            hold on
            if rel_use_vel
                plot(rel_vel_res_t, rel_vel_res_x, 'rx',...
                     rel_vel_res_t, rel_vel_res_y, 'gx',...
                     rel_vel_res_t, rel_vel_res_z, 'bx')
            else
                plot(rel_vel_res_t, rel_vel_res_x, 'rd',...
                     rel_vel_res_t, rel_vel_res_y, 'gd',...
                     rel_vel_res_t, rel_vel_res_z, 'bd')
            end
            hold off
        end
        
        xlabel('t (s)')
        ylabel('(m/s)')
        grid on
        title(sprintf('N VEL STDEV: %0.5f    E VEL STDEV: %0.5f    D VEL STDEV: %0.5f', agps_Nv_stdev, agps_Ev_stdev, agps_Dv_stdev))
    end
    %
    if plot310
        tab310 = uitab(tabgp3,'Title','Ship Mag');
        hax310 = axes('Parent',tab310);
        plot_sigma_line(smag_t, smag_x, smag_t, smag_xstdev, false, 'r-', red_sig_color, 1, true)
        plot_sigma_line(smag_t, smag_y, smag_t, smag_ystdev, false, 'g-', grn_sig_color, 1, true)
        plot_sigma_line(smag_t, smag_z, smag_t, smag_zstdev, false, 'b-', blu_sig_color, 1, false)
        
        if has_rel_rhd_res
            hold on
            if rel_use_mag
                plot(rel_rhd_res_t, rel_rhd_res_x, 'rx',...
                     rel_rhd_res_t, rel_rhd_res_y, 'gx',...
                     rel_rhd_res_t, rel_rhd_res_z, 'bx')
            else
                plot(rel_rhd_res_t, rel_rhd_res_x, 'rd',...
                     rel_rhd_res_t, rel_rhd_res_y, 'gd',...
                     rel_rhd_res_t, rel_rhd_res_z, 'bd')
            end
            hold off
        end
        
        xlabel('t (s)')
        ylabel('Unit Vector')
        grid on
        title(sprintf('STDEV: %0.5f    BIAS RANGE: %0.5f    BIAS WALK STDEV: %0.5f', mag_stdev, mag_b_rng, mag_b_stdev))
    end
end

function [] = plot_sigma_line(t, data, t_sigma, sigma, interpolate_data, linespec, sig_color, linewidth, hold_on)

if ~isrow(t)
    t = t';
end
if ~isrow(t_sigma)
    t_sigma = t_sigma';
end
if ~isrow(data)
    data = data';
end
if ~isrow(sigma)
    sigma = sigma';
end

if interpolate_data
    sigma_interp = interp1(t_sigma', sigma', t')';
else
    sigma_interp = sigma;
end

x_vector = [t, fliplr(t)];
patch = fill(x_vector, [data + sigma_interp, fliplr(data - sigma_interp)], sig_color);
set(patch, 'edgecolor','none');
set(patch, 'FaceAlpha', 0.2);
hold on
plot(t, data, linespec, 'Linewidth', linewidth)

if ~hold_on
    hold off
end

end

function [indices, tdata_filt] = indices_between(tdata, t0, tf)

indices = logical((tdata >= t0) .* (tdata <= tf));
tdata_filt = tdata(indices);

end