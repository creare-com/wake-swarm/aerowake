addpath(genpath('matlab_utilities/'));

bag1 = 'ekf_dc_trial_1_EKF_true_2020-08-14-12-24-55.bag';
bag2 = 'ekf_dc_trial_1_EKF_false_2020-08-14-12-27-15.bag';

bag1data = processAllROSBagTopics(strcat('../bags/ekf_dc_08-06-2020/',bag1),false);
bag2data = processAllROSBagTopics(strcat('../bags/ekf_dc_08-06-2020/',bag2),false);

t_rp_est = bag1data.est_rel_pose.t;
x_rp_est = bag1data.est_rel_pose.pose.position(1,:);
y_rp_est = bag1data.est_rel_pose.pose.position(2,:);
z_rp_est = bag1data.est_rel_pose.pose.position(3,:);
t_rp_tru = bag2data.est_rel_pose.t;
x_rp_tru = bag2data.est_rel_pose.pose.position(1,:);
y_rp_tru = bag2data.est_rel_pose.pose.position(2,:);
z_rp_tru = bag2data.est_rel_pose.pose.position(3,:);

figure(1)
plot(t_rp_est, x_rp_est, 'r--', t_rp_tru, x_rp_tru, 'r-', ...
     t_rp_est, y_rp_est, 'g--', t_rp_tru, y_rp_tru, 'g-', ...
     t_rp_est, z_rp_est, 'b--', t_rp_tru, z_rp_tru, 'b-')
grid on
title('Estimated Relative Position Between Ship and UAV')
xlabel('Time (seconds)')
ylabel('Distance (meters)')
legend('Estimated','Truth')
     

t_qREL = bag1data.q_NED_REL.t;
phi_qREL = bag1data.q_NED_REL.euler(1,:);
tht_qREL = bag1data.q_NED_REL.euler(2,:);
psi_qREL = bag1data.q_NED_REL.euler(3,:);

figure(2)
plot(t_qREL, 180*phi_qREL/pi, 'r-', ...
     t_qREL, 180*tht_qREL/pi, 'g-', ...
     t_qREL, 180*psi_qREL/pi, 'b-')
grid on
title('Estimated Rotation from NED to Ship Frame')
xlabel('Time (seconds)')
ylabel('Euler Angle (degrees)')
legend('Roll','Pitch','Yaw')

