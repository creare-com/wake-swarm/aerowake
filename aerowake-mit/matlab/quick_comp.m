beforebag = 'before_2020-04-24-16-07-37.bag';
afterbag = 'after_2020-04-24-16-08-22.bag';

close all
% addpath(genpath('matlab_utilities/'));
% beforedata = processAllROSBagTopics(beforebag, false);
% afterdata = processAllROSBagTopics(afterbag, false);

before_tmin = 10;
before_tmax = 15;
before_A_t = beforedata.hw_odometry.t;
bA_idx = logical((before_A_t < before_tmax) .* (before_A_t > before_tmin));
before_A_t = before_A_t(bA_idx);
before_A_x = beforedata.hw_odometry.pose.position(1,bA_idx);
before_A_y = beforedata.hw_odometry.pose.position(2,bA_idx);
before_A_z = beforedata.hw_odometry.pose.position(3,bA_idx);
before_B_t = beforedata.aerowake_MIT.truth.NED.t;
bB_idx = logical((before_B_t < before_tmax) .* (before_B_t > before_tmin));
before_B_t = before_B_t(bB_idx);
before_B_x = beforedata.aerowake_MIT.truth.NED.pose.position(1,bB_idx);
before_B_y = beforedata.aerowake_MIT.truth.NED.pose.position(2,bB_idx);
before_B_z = beforedata.aerowake_MIT.truth.NED.pose.position(3,bB_idx);

after_tmin = 10;
after_tmax = 15;
after_A_t = afterdata.hw_odometry.t;
aA_idx = logical((after_A_t < after_tmax) .* (after_A_t > after_tmin));
after_A_t = after_A_t(aA_idx);
after_A_x = afterdata.hw_odometry.pose.position(1,aA_idx);
after_A_y = afterdata.hw_odometry.pose.position(2,aA_idx);
after_A_z = afterdata.hw_odometry.pose.position(3,aA_idx);
after_B_t = afterdata.aerowake_MIT.truth.NED.t;
aB_idx = logical((after_B_t < after_tmax) .* (after_B_t > after_tmin));
after_B_t = after_B_t(aB_idx);
after_B_x = afterdata.aerowake_MIT.truth.NED.pose.position(1,aB_idx);
after_B_y = afterdata.aerowake_MIT.truth.NED.pose.position(2,aB_idx);
after_B_z = afterdata.aerowake_MIT.truth.NED.pose.position(3,aB_idx);

subplot(1,2,1)
plot(before_A_t, before_A_x, 'r-', before_A_t, before_A_y, 'g-', before_A_t, before_A_z, 'b-', ...
     before_B_t, before_B_x, 'r--', before_B_t, before_B_y, 'g--', before_B_t, before_B_z, 'b--')
grid on
xlabel('Time (s)')
ylabel('Position (m)')
title('Before Parameter Matching')
legend('Hardware x-position','Hardware y-position','Hardware z-position','Simulated x-position','Simulated y-position','Simulated z-position','Location','Southwest')

subplot(1,2,2)
plot(after_A_t, after_A_x, 'r-', after_A_t, after_A_y, 'g-', after_A_t, after_A_z, 'b-', ...
     after_B_t, after_B_x, 'r--', after_B_t, after_B_y, 'g--', after_B_t, after_B_z, 'b--')
title('After Parameter Matching')
grid on
xlabel('Time (s)')