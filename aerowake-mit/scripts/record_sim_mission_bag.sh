#!/bin/bash

if [[ "$1" = "-h" || "$1" = "--help" ]]; then
  echo "record_sim_mission_bag.sh BAGNAME BAGSUBPATH"
  exit
fi

BAGNAME="$1"

if [[ -z "$BAGNAME" ]]; then
  echo "BAGNAME not set! Exiting..."
  exit
fi

BAGSUBPATH="$2"

if [[ -z "$BAGSUBPATH" ]]; then
  echo "BAGSUBPATH not set; using default (empty) subpath..."
  BP="bags"
else
  BP="bags/$BAGSUBPATH"
fi

##################################################
# GET DIRECTORY OF THIS SCRIPT TO ACCESS RESOURCES
##################################################

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#########
# EXECUTE
#########

source "$DIR/../sim_ws/sourceror.sh"
SIMPATH=`rospack find aerowake_sim`
BAGPATH="$SIMPATH/$BP"
[ ! -d "$BAGPATH" ] && mkdir -p "$BAGPATH"

rosparam dump "$BAGPATH/$BAGNAME.yaml"
rosbag record -O "$BAGPATH/$BAGNAME.bag" /rel/imu_bias/throttled /abs/imu_bias/throttled /rel_pose/rel_res/throttled /relative_heading/rel_res/throttled /pos/rel_res/throttled /baro/rel_res/throttled /vel/rel_res/throttled /att/abs_res/throttled /vel/abs_res/throttled /gnss/abs_res/throttled /baro/abs_res/throttled /ship_mag/throttled /rover/navvelned/throttled /rover/navrelposned/throttled /rover/navpvt/throttled /vision_pose/throttled /odometry/throttled /attitude/throttled /imu/data/throttled /full_hl_command/throttled /command/throttled /air_wrench/throttled /tether_wrench/throttled /uav_ext_wrench/throttled /uav_truth_NED/throttled /Fext/throttled /aerowake_uav/truth/NED/throttled /aerowake_uav/rel_truth/NED/throttled /rel_odometry/throttled /baro/throttled /base_vel/rel_res/throttled
